<?php

use yii\db\Migration;

/**
 * Class m220226_060530_variasi
 */
class m220226_060530_variasi extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220226_060530_variasi cannot be reverted.\n";

        return false;
    }
    */

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('variasi', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull()
        ]);
    }

    public function down()
    {
        echo "m220226_060530_variasi cannot be reverted.\n";

        return false;
    }
}
