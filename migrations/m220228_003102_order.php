<?php

use yii\db\Migration;

/**
 * Class m220228_003102_order
 */
class m220228_003102_order extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220228_003102_order cannot be reverted.\n";

        return false;
    }
    */

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'id_barang_desc' => $this->integer()->null(),
            'alamat_pengiriman' => $this->string()->notNull(),
            'qty' => $this->integer()->notNull(),
            'id_info_toko' => $this->integer()->null(),
            'id_metode_pembayaran' => $this->integer()->null(),
            'id_metode_pengiriman' => $this->integer()->null(),
            'id_pelanggan' => $this->integer()->null()
        ]);

        $this->addForeignKey(
            'fk-order-id_barang_desc',
            'order',
            'id_barang_desc',
            'barang_desc',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-id_info_toko',
            'order',
            'id_info_toko',
            'info_toko',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-id_metode_pembayaran',
            'order',
            'id_metode_pembayaran',
            'metode_pembayaran',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-id_metode_pengiriman',
            'order',
            'id_metode_pengiriman',
            'metode_pengiriman',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-id_pelanggan',
            'order',
            'id_pelanggan',
            'users',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m220228_003102_order cannot be reverted.\n";

        return false;
    }
}
