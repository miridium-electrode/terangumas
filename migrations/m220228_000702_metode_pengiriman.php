<?php

use yii\db\Migration;

/**
 * Class m220228_000702_metode_pengiriman
 */
class m220228_000702_metode_pengiriman extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220228_000702_metode_pengiriman cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('metode_pengiriman', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'id_info_toko' => $this->integer()->null()
        ]);

        $this->addForeignKey(
            'fk-metode_pengiriman-id_info_toko',
            'metode_pengiriman',
            'id_info_toko',
            'info_toko',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m220228_000702_metode_pengiriman cannot be reverted.\n";

        return false;
    }
}
