<?php

use yii\db\Migration;

/**
 * Class m220227_234120_barang_desc
 */
class m220227_234120_barang_desc extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220227_234120_barang_desc cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('barang_desc', [
            'id' => $this->primaryKey(),
            'id_barang' => $this->integer()->null(),
            'id_jenis' => $this->integer()->null(),
            'id_variasi' => $this->integer()->null(),
            'gambar_barang' => $this->string()->null()
        ]);

        $this->addForeignKey(
            'fk-barang_desc-id_barang',
            'barang_desc',
            'id_barang',
            'barang',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-barang_desc-id_jenis',
            'barang_desc',
            'id_jenis',
            'jenis',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-barang_desc-id_variasi',
            'barang_desc',
            'id_variasi',
            'variasi',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m220227_234120_barang_desc cannot be reverted.\n";

        return false;
    }
}
