<?php

use yii\db\Migration;

/**
 * Class m220227_235906_kontak
 */
class m220227_235906_kontak extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220227_235906_kontak cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('kontak', [
            'id' => $this->primaryKey(),
            'kontak_info' => $this->string()->notNull(),
            'id_info_toko' => $this->integer()->null()
        ]);

        $this->addForeignKey(
            'fk-kontak-id_info_toko',
            'kontak',
            'id_info_toko',
            'info_toko',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m220227_235906_kontak cannot be reverted.\n";

        return false;
    }
}
