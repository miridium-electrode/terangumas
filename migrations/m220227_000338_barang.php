<?php

use yii\db\Migration;

/**
 * Class m220227_000338_barang
 */
class m220227_000338_barang extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220227_000338_barang cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('barang', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'stok' => $this->integer()->defaultValue(0),
            'harga' => $this->decimal(8, 2)->notNull()
        ]);
    }

    public function down()
    {
        echo "m220227_000338_barang cannot be reverted.\n";

        return false;
    }
    
}
