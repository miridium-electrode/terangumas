<?php

use yii\db\Migration;

/**
 * Class m220227_001053_jenis
 */
class m220227_001053_jenis extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220227_001053_jenis cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('jenis', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull()
        ]);
    }

    public function down()
    {
        echo "m220227_001053_jenis cannot be reverted.\n";

        return false;
    }
}
