<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220226_053335_users
 */
class m220226_053335_users extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220226_053335_users cannot be reverted.\n";

        return false;
    }
    */
    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('users', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING. ' NOT NULL',
            'role' => Schema::TYPE_STRING. ' NOT NULL'
        ]);
    }

    public function down()
    {
        echo "m220226_053335_users cannot be reverted.\n";

        return false;
    }
    
}
