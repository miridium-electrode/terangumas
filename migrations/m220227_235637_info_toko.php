<?php

use yii\db\Migration;

/**
 * Class m220227_235637_info_toko
 */
class m220227_235637_info_toko extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220227_235637_info_toko cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('info_toko', [
            'id' => $this->primaryKey(),
            'alamat' => $this->string()->notNull(),
            'profile_pic' => $this->string()->null()
        ]);
    }

    public function down()
    {
        echo "m220227_235637_info_toko cannot be reverted.\n";

        return false;
    }
}
