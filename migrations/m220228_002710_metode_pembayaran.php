<?php

use yii\db\Migration;

/**
 * Class m220228_002710_metode_pembayaran
 */
class m220228_002710_metode_pembayaran extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m220228_002710_metode_pembayaran cannot be reverted.\n";

        return false;
    }
    */

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('metode_pembayaran', [
            'id' => $this->primaryKey(),
            'nama' => $this->string()->notNull(),
            'id_info_toko' => $this->integer()->null()
        ]);

        $this->addForeignKey(
            'fk-metode_pembayaran-id_info_toko',
            'metode_pembayaran',
            'id_info_toko',
            'info_toko',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m220228_002710_metode_pembayaran cannot be reverted.\n";

        return false;
    }
}
