<?php

namespace app\controllers;

class LandingPageController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->renderPartial('index');
    }

}
